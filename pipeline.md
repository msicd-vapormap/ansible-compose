# Déploiement de la stack Docker Compose de Vapormap sur une instance cloud avec Ansible à partir d'un pipeline sur Gitlab  

__Par__: FETCHEPING FETCHEPING Rossif Borel  
__Email__: rossifetcheping@outlook.fr  
__École__: IMT Atlantique  
__Formation__: Mastère spécialisé Infrastructures Cloud et DevOps  
__Année académique__: 2022-2023  
__UE__: Projet fil rouge  
__Dépôt Gitlab__: <https://gitlab.imt-atlantique.fr/vapormap-cicd/ansible-compose>  
__Pages Gitlab__: <https://vapormap-cicd.gitlab-pages.imt-atlantique.fr/ansible-compose>  
__Date__: 27 Février 2023  

## Prérequis

* La disponibilité des images Docker de la stack Compose dans la registry du projet "docker"  
* La disponibilité de l'image Docker ansible-msicd:1.0 du projet "tools"  
* Le token d'accès à la registry Docker du groupe vapormap-cicd  
* Le token d'accès au dépot du projet "docker"  
* Le déploiement de l'infrastructure du projet "terraform"  
* Le package registry du projet "terraform":  
  * la disponibilité du fichier __public_ip.ini__ du package  
  * la disponibilité du fichier __hosts.ini__ du package  
* La clé privée SSH de connexion aux instances  

## Configurer les variables Ansible

* Modifier le fichier __group_vars/all/main.yml__ pour personnaliser les tâches à effectuer par Ansible.  

> Remarque: Ne pas toucher au fichier __group_vars/all/env.yml.template__. Il est configuré dans la chaine de déploiement.  

## Configurer l'environnement du pipeline sur Gitlab

### Créer ou mettre à jour les variables Gitlab du projet "ansible-compose"

* Se rendre dans l'onglet "Settings > CI/CD" du projet.  
* Dans la section "Variables", modifier/ajouter les variables suivantes:  
  * __TAG_IMAGE_API__: tag de l'image Docker de l'API  
  * __TAG_IMAGE_FRONT__: tag de l'image Docker du Frontend  
  * __TAG_IMAGE_DB__: tag de l'image Docker de la base de données  
  * __DB_NAME__: nom de la base de données  
  * __DB_USER__: nom d'utilisateur de la base de données  
  * __DB_PASS__: mot de passe de la base de données  

### Créer ou mettre à jour les variables Gitlab du groupe "vapormap-cicd"

* Se rendre dans l'onglet "Settings > CI/CD" du groupe.  
* Dans la section "Variables", modifier/ajouter les variables suivantes:  
  * __PRIVATE_KEY__: contenu de la clé privée SSH (Utiliser une variable de type "File")  
  * __BASTION_PUBLIC_IP__: adresse IP publique de l'instance bastion (_Update: la variable est mise à jour automatiquement via le pipeline du projet Terraform_)  
  * __NODE_PUBLIC_IP__: adresse IP publique du noeud manager (_Update: la variable est mise à jour automatiquement via le pipeline du projet Terraform_)  
  * __BASTION_USER__: utilisateur SSH bastion  
  * __NODE_USER__: utilisateur SSH des noeuds  
  * __TF_PROJECT_ID__: identifiant du projet Terraform  
  * __TF_PKG_NAME__: nom du package registry des fichiers d'inventaire générés par Terraform.  
  * __TF_PKG_VERSION__: version du package registry
  * __DOCKER_PROJECT_ID__: identifiant du projet Docker  
  * __TOKEN_DOCKER_REPO__: token d'accès en lecture au dépôt du projet Docker  
  * __TOKEN_REGISTRY_VAP__: token d'accès en lecture à la registry du groupe vapormap-cicd  

## Vérifier l'inventaire

Il faut s'assurer que le fichier d'inventaire __hosts.ini__ de l'infrastructure est disponible dans le package registry __vapormap_pkg_tf__ du projet "terraform".  

## Déployer Vapormap sur le noeud manager

* Se rendre dans l'onglet "CI/CD > pipeline" du projet.  
* Cliquer sur le bouton __"Run pipeline"__.  
* Sur la fénètre qui s'affiche, cliquer à nouveau sur le bouton __"Run pipeline"__ pour valider l'opération sans renseigner de variables.  

## Tester l'application

Depuis un navigateur : <http://PUBLIC_IP_MANAGER:8000>

## Maintenance de Vapormap sur le noeud manager

Dans le pipeline précédent,  

* déclencher le job __stop_vapormap__ pour arrêter la stack Vapormap  
* déclencher le job __restart_vapormap__ pour redémarrer la stack Vapormap  
* déclencher le job __destroy_vapormap__ pour supprimer l'installation et nettoyer l'instance  
