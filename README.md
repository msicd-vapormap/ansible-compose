# Déploiement de la stack Docker Compose de Vapormap sur une instance cloud avec Ansible

__Par__: FETCHEPING FETCHEPING Rossif Borel  
__Email__: rossifetcheping@outlook.fr  
__École__: IMT Atlantique  
__Formation__: Mastère spécialisé Infrastructures Cloud et DevOps  
__Année académique__: 2022-2023  
__UE__: Projet fil rouge  
__Dépôt Gitlab__: <https://gitlab.imt-atlantique.fr/vapormap-cicd/ansible-compose>  
__Pages Gitlab__: <https://vapormap-cicd.gitlab-pages.imt-atlantique.fr/ansible-compose>  
__Date__: 27 Février 2023  

## Description

Ce projet permet de déployer et maintenir automatiquement la stack Docker Compose de l'application Vapormap sur une instance Linux dotée d'un environnement Docker.  

* L'infrastruction du projet "terraform" du groupe "vapormap-cicd" doit être préalablement mise en place.  

* L'inventaire de l'infrastructure est généré par le projet "terraform"  

* Les opérations sont effectuées à partir des playbooks Ansible.  

* Les tâches à exécuter sont organisées dans des rôles.  

* Les playbooks sont variabilisés pour permettre la personnalisation de l'environnement de production.  

* L'un des fichiers de variables Ansible est généré à partir du template __group_vars/all/env.yml.template__ et des variables d'environnement.  

* La connexion SSH aux instances est configurée à l'aide du fichier __ssh.conf.template__ et des variables d'environnement. Le fichier __ssh.conf__ va être généré et utilisé par la configuration d'Ansible.  

* La stack Docker Compose est disponible dans le projet "docker" et téléchargée lors du déploiement via Ansible grâce à un token d'accès à la registry.  

* Les images Docker de la stack sont dans la registry du projet "docker" et accessibles avec un token d'accès au dépôt.  

* Le fichier .env des variables de la stack Compose est templaté en Jinja et contenu dans le rôle __docker_compose__. Il est configurable grâce aux variables Ansible du projet.  

## Utilisation du dépôt

Pour déployer l'application sur noeud manager de l'infrastruture, deux modes opératoires sont disponibles :  

* [Déploiement depuis un poste local](./local.md)  
* [Déploiement depuis un pipeline Gitlab CD](./pipeline.md)  

## Présentation de l'infrastructure Cloud

* L'infrastructure est constituée de trois noeuds contenus dans un réseau privé accessible en SSH à partir d'une instance bastion. Seul le noeud manager sera exploité pour ce déploiement.  
* Le noeud manager est rattaché à une IP flottante et expose des ports sur internet pour rendre l'application accessible. C'est le noeud sur lequel la stack est déployé.  
* L'instance bastion est rattachée à une IP flottante et expose le port SSH sur internet pour permettre la configuration du noeud et le déploiement de l'application à partir d'Ansible.  
