lint:
	@rm -rf lint && mkdir lint
	@ansible-lint > lint/ansible.log
	@yamllint . > lint/yamllint.log
